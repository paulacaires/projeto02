media: estatistica.o
	gcc media.o estatistica.o -o media

estatistica.o: estatistica.c estatistica.h
	gcc -c estatistica.c

clean:
	rm media *.o
