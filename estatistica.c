#include "estatistica.h"

T media (int n, T *v) {
  T sum;

  for (int i = 0; i < n; i++) 
    sum += v[i];

  return (sum / n);
}
