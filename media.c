#include <stdio.h>
#include "estatistica.h"
#define MAXN 256

typedef double T;

int main () {
  T v[MAXN];
  int n;

  scanf ("%d", &n);

  for (int i = 0; i < n; i++) 
    scanf ("%lf", &v[i]);

  printf ("%lf\n", media (n, v));


  return (0);
}
